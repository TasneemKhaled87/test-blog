<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    protected $fillable = [
       'photo','author_id','category_id', 'title','description',
    ];

    //this book related to the category

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

 public function author()
    {
        return $this->belongsTo('App\Author');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function getphoto($photo)
    {
        return asset($photo);
    }
}