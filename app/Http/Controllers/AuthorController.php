<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;

class AuthorController extends Controller
{
   
    public function index()
    {
        //
        return view('authors.index')->with('authors',Author::all());

    }

   
    public function create()
    {
        //
        return view('authors.create');

    }

  
    public function store(Request $request)
    {
        //
       // $this->validate($request,[
      //   "name" => "required",
     //    "description" =>"required"
       //  ]);
 
      //   $author=new Author;
      //   $author->name = $request->name;
     //    $author->description = $request->description;

       //  $author->save();
 
      //   return redirect()->back();

         $validatedData = $request->validate([
            'name' => 'required|max:255',
            "description" =>"required||min:20|max:255"
           
        ]);

        Author::create($request->all());

        return redirect()->back();

 
    }

   
    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
        //  //
       $author= Author::find($id);
       return view('authors.edit')->with('author',$author);

    }

 
    public function update(Request $request, $id)
    {
        //
      //  $author= Author::find($id); 
    // $author->name = $request->name;
     //   $author->description = $request->description;
      //  $author->save();
    //    return redirect()->route('authors');

        $author = Author::find($id);
        $author->update($request->all());
        return redirect()->route('authors');


    }

  
    public function destroy($id)
    {
        //
        $author= Author::find($id); 
        $author->delete();

        return redirect()->route('authors');

    }
}
