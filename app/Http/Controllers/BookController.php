<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Book;
use App\Author;
use App\User;
use App\Photo;
use Validator;
class BookController extends Controller
{
//disblay
    public function index()
    {
        //
        return view('books.index')->with('books',Book::all());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       return view('books.create')-> with('categories',Category::all())-> with('authors',Author::all());
    }

    /**
     * Store a newly created resource in storage.
   
     */
    public function store(Request $request)
    {
        //
    //    $this->validate($request,[
      //    "title" => "required",
       //     "description" => "required",
        //    "photo_id" => "required",
          //  "author_id" => "rquired",
           // "category_id" => "required",
        
    //  ]);

     //   $book = new Book();
     //   $book->title = $request->title;
     //   $book->description = $request->description;
    //    $book->photo_id = $request->photo_id;
   //     $book->author_id = $request->author_id;
    //    $book->category_id = $request->category_id;
//
      //  $book->save();

    //    $photo_id = $request ->photo_id;
    //    $photo_new_name = time().$photo_id->getClientOriginalName();
    //    $photo_id->move('uploads/images',$photo_new_name);

    //    $book=Book::create([
    //      "title" => $request->title,
      //    "description" => $request->description,
        //  "photo_id" =>'uploads/images',
       //   "author_id" => $request->author_id,
        //  "category_id" => $request->category_id,

        //]);

        //return redirect()->back();
       // dd(request()->all());
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            "description" => "required|max:255",
            "photo" => "required|image",
            "author_id" => "required",
            "category_id" => "required",
        ]);

        $photo= $request ->photo;
        $photo_new=time().$photo->getClientOriginalName();
        $photo->move('uploads/images',$photo_new);

        $book=Book::create([
           "title" => $request->title,
           "description" => $request->description,
            "photo" =>'uploads/images/'.$photo_new,
            "author_id" => $request->author_id,
            "category_id" => $request->category_id,

        ]);
          // Book::create($request->all());
            //if($request->hasFile('photo')){
             //   $request->photo->store('/uploads/images');
          //  }
    
         return redirect()->back();
        
     // dd($request->all());

    }

    /**
     * Display the specified resource.
    
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
  
     */
    public function edit($id)
    {
        //
        $book= Book::find($id);
        return view('books.edit')->with('books',$book)->with('categories',Category::all())->
        with('photos',Photo::all())->with('authors',Author::all());
 
      
    }

    /**
     * Update the specified resource in storage.
    
     */
    public function update(Request $request, $id)
    {
        //
       //     $this->validate($request,[
     //      "title" => "required",
       //     "description" => "required",
     //        "photo_id" => "required",
     //        "author_id" => "required",
      //       "category_id" => "required",
    //    ]);

     //   $book = Book::findOrFail($id);
       // $book->update($request->all());
       // return redirect('books');

       $book = Book::findOrFail($id);

       $this->validate($request,[
              "title" => "required",
               "description" => "required",
            //"photo_id" => "required",
               "author_id" => "required",
               "category_id" => "required",
        ]);


          if ($request->hasFile('photo') ){
            $photo= $request ->photo;
            $photo_new=time().$photo->getClientOriginalName();
            $photo->move('uploads/images',$photo_new);
            $book->photo =$photo_new;
          }

         $book->title =$request->title;
         $book->description = $request->description;
       //   $book->photo ='uploads/images/'.$photo_new,
          $book->author_id = $request->author_id;
          $book->category_id = $request->category_id;
         
          $book->save();
          return redirect('/books');
      //  $book = $request->all();
        //if($file = $request->file('photo_id')){
            //$file_name = time() . $file->getClientOriginalName();
           // $file->move('/uploads/images', $file_name);
         //   $photo = Photo::create(['file'=>$file_name]);
       //     $book['photo_url'] = $photo->id;
      //  }
       // return redirect('/books');
    
        }

    /**
     * Remove the specified resource from storage.
    
     */
    public function destroy($id)
    {
        //
        $book= Book::find($id); 
        $book->delete();

        return redirect()->route('books');

    }
}
