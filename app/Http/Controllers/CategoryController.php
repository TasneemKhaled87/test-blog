<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;


class CategoryController extends Controller
{
    
    public function index()
    {
        //
       return view('categories.index')->with('categories',Category::all());

    }

  
    public function create()
    {
        //
        return view('categories.create');
    }

 
    public function store(Request $request)
    {
        //
       //$this->validate($request,[
       //    "name" => "required"
     //   ]);

      //  $category=new Category;
      //  $category->name = $request->name;
     //   $category->save();

       // return redirect()->back();


       $validatedData = $request->validate([
        'name' => 'required|max:255',
       ]);
       
        Category::create($request->all());
        return redirect()->back();
        
     // dd($request->all());
    }

  
    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
        //
       $category= Category::find($id);
       return view('categories.edit')->with('category',$category);
  
    }

  
    public function update(Request $request, $id)
    {
        //
       // $category= Category::find($id); 
       // $category->name = $request->name;
        //$category->save();

       // return redirect()->route('categories');

        $category = Category::findOrFail($id);
        $category->update($request->all());
        return redirect('categories');

    }

  
    public function destroy($id)
    {
        //
        $category= Category::find($id); 
        $category->delete();

        return redirect('categories');

    }
}
