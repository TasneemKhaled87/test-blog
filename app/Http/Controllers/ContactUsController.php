<?php

namespace App\Http\Controllers;
use App\Notifications\SendContactNotification;
use Illuminate\Http\Request;
use Illuminate\Http\Notification;

use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notifications;


//use Illuminate\Support\Facades\Notification;
use Session;
use App\ContactUS;

class ContactUsController extends Controller
{
    //show form 
   
    public function show()
    {
        return view('contacts.form');
    }


    //sending mail
    public function sendEmail(Request $request)
    {
        $this->validate($request, [
         'name' => 'required',
         'email' => 'required|email',
         'message' => 'required'
         ]);

         Notificaion::route('mail','test@gmail.com')
                     ->notify(new SendContactNotification($request));

          Session::flash('success', 'the mail sent successfully');

         return back();

        }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //  $this->validate($request, [
       //  'name' => 'required',
       //  'email' => 'required|email',
        // 'message' => 'required'
      //   ]);
     //   $contactus=new ContactUs;
     //   $contactus->name = $request->name;
       // $contactus->email = $request->email;
     //   $contactus->message = $request->message;

      //  $contactus->save();

        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email',
            'message' => 'required|string|max:255'
        ]);
            ContactUS::create($request->all());
    
            return redirect()->back();
            
    
        //ContactUS::create($request->all());
//        $contactus->save();

   //     Mail::send('email',
     //      array(
       //        'name' => $request->get('name'),
         //      'email' => $request->get('email'),
           //    'user_message' => $request->get('message')
           //), function($message)
       //{
         //  $message->from('sas@gmail.com');
          // $message->to('ssss@cloudways.com', 'Admin')->subject('Cloudways Feedback');
       //});
        //return back()->with('success', 'Thanks for contacting us!');
    }
}

