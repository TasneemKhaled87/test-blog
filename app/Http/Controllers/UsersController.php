<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UsersController extends Controller
{
  
    public function index()
    {
        //
        return view('users.index')->with('users',User::all());

    }


    public function create()
    {
        //
        return view('users.create');

    }

    public function store(Request $request)
    {
        //
       // $this->validate($request,[
        //    "name" => "required",
         //   "email" => "required",
         //   "email_verified_at" =>"",
        // "password" => "required",
        //    "gender" => "required"

     //    ]);
    
    
         // $user=new User;
        // $user->name = $request->name;
        //$user->email = $request->email;
        // $user->email_verified_at = $request->email_verified_at;
        // $user->password = $request->password;
        // $user->gender = $request->gender;

        // $user->save();

         $validatedData = $request->validate([
            'name' => 'required|max:255',
            "email" => 'required|unique:users||max:50',
            "email_verified_at" =>'',
            "password" => "required|min:6|max:10",
            "gender" => "required"
        ]);

        User::create($request->all());

        $profile=Profile::create([
            'user_id'=>$user->id
        ]);
        return redirect()->back();

 
      // dd($request->all());
    }


    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
        $user= User::find($id);
        return view('users.edit')->with('user',$user);
 
      
    }

  
    public function update(Request $request, $id)
    {
        //
      //  $user= User::find($id); 
        //$user->name = $request->name;
       //$user->email = $request->email;
       //$user->email_verified_at = $request->email_verified_at;
      //$user->password = $request->password;
      // $user->gender = $request->gender;
      //$user->save();

       // return redirect()->route('users');

        $user= User::findOrFail($id); 
        $user->update($request->all());
        return redirect('/users');

        

    }
    

    public function destroy($id)
    {
        //
        $user= User::find($id); 
        $user->delete();

        return redirect('users');

    }

    public function admin($id)
    {
        //
        $user= User::find($id); 
        $user->admin=1;
        $user->save();


        return redirect('users');

    }

    public function notAdmin($id)
    {
        //
        $user= User::find($id); 
        $user->admin=0;
        $user->save();


        return redirect('users');

    }
}
