<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    //
    protected $fillable = [ 'photo_url'];

    public function books()
    {
        return $this->hasOne('App\Book');
    }
}
