<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $fillable = [
        'avater', 'user_id', 'facebook',
    ];

    public function user()
    {
        return $this->belongsTo('App\Profile');
    }
}
