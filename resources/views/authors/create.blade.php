@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Author :</div>
<p>

                <div class="card-body">
           

                @if(count ($errors) > 0)
                     <ul  class="navbar-nav mr-auto">
                         @foreach ($errors->all() as $error)
                         <li class="nav-item active">
                            {{$error}}
                         </li>
                        @endforeach
                    </ul>
                    @endif


                    <form action="{{route('author.store')}}" method="post" >
                          {{ csrf_field()}}

                      <div class="form-group">
                             <label for="name">Name of Author:</label>
                          <input type="text" class="form-control" name="name" placeholder="author name" />
                     </div>

                     <div>
                       <label for="description">Description:</label>
                       <textarea class="form-control" cols="40" rows="6" name="description"></textarea>
                     </div>


<p>
           
<hr>

                    <div>
                       <button type="submit" class="addbtn"> Add Author</button>

                    </div>

</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
