@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Author {{$author->name}}</div>
<p>

                <div class="card-body">
           

                    <form action="{{route('author.update',['id'=>$author->id])}}" method="post" >
                          {{ csrf_field()}}
                      <div>
                             <label for="name">Name of Author:</label>
                          <input type="text" class="form-control" name="name" value="{{$author->name}}" />
                     </div>

 <div>
                       <label for="description">Description:</label>
                       <textarea class="form-control" cols="40" rows="6" name="description" value="{{$author->description}}"></textarea>
                     </div>

<p>
           
<hr>

                    <div>
                       <button type="submit" class="addbtn" href="">Update Author</button>

                    </div>

</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
