@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Authors :</div>
<p>

                <div class="card-body">
           

                <table class="table table-borderless table-dark">
                       <thead>
                          <tr>
                              <th scope="col">No.of.Author</th>
                              <th scope="col">Info.Author</th>
                              <th scope="col">Edit</th>
                              <th scope="col">Delete</th>
 
                          </tr>
                     </thead>
                     <tbody>
                        @foreach ($authors as $author)
                         <tr>
                              <th scope="row">{{$author->name}} </th>
                              <th scope="row">{{$author->description}}</th>
                               <td>
                               <a class="" href="{{route('author.edit',['id'=>$author->id])}}"><i class="fas fa-edit"></li>Edit</a>

                               </td>

                               <td>
                               <a class="" href="{{route('author.delete',['id'=>$author->id])}}"><i class="far fa-trash-alt"></i>Delete</a>

                               </td>
                         </tr>
                      
                        @endforeach
                     </tbody>
                </table>




                </div>
            </div>
        </div>
    </div>
</div>
@endsection