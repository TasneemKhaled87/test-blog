@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Books {{$books->title}} </div>
<p>

                <div class="card-body">
           

                  @if(count ($errors) > 0)
                     <ul  class="navbar-nav mr-auto">
                       @foreach ($errors->all() as $error)
                         <li class="nav-item active">
                            {{$error}}
                         </li>
                        @endforeach
                    </ul>
                    @endif

                    <form action="{{route('book.update',['id'=>$books->id])}}" method="POST" enctype="multipart/form-data"  > 
          
                          {{ csrf_field()}}
                       
                      <div>
                             <label for="title">Title of book:</label>
                          <input type="text" name="title" value="{{$books->title}}" />
                     </div>


<p>

                  <div>
                       <label for="description">Description:</label>
                       <textarea class="form-control" cols="40" rows="6" name="description">
                          {{$books->description}}
                       </textarea>
                     </div>


   <p>
                     <div class="form-group">
                       <label for="photo">Input Photo:</label>
                       <input type="file" class="form-control " name="photo" value="{{$books->photo}}">
                    </div>
<p>
                    

    

                     <div class="form-group">
                            <label for="select-choice">Select Author:</label>
                             <select class="form-control"  name="author_id" >
                             
                               @foreach ($authors as $author)
                                <option value="{{$author->id}}">{{$author->name}}</option>
                                @endforeach
                           

                            </select>
                     </div>
<br>


                      <div class="form-group">
                            <label for="select-choice">Select Dropdown Category:</label>
                             <select class="form-control" id="category" name="category_id">
                             
                               @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                           

                            </select>
                     </div>


<p>

<p>
                   
<hr>

                    <div>
                       <button type="submit" class="addbtn" href="">Update Book</button>

                    </div>

</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
