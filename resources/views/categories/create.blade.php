@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Category :</div>
<p>

                <div class="card-body">
           

                @if(count ($errors) > 0)
                     <ul  class="navbar-nav mr-auto">
                         @foreach ($errors->all() as $error)
                         <li class="nav-item active">
                            {{$error}}
                         </li>
                        @endforeach
                    </ul>
                    @endif


                    <form action="{{route('category.store')}}" method="post" >
                          {{ csrf_field()}}
                      <div class="form-group">
                             <label for="name">Name of Category:</label>
                          <input type="text" class="form-control" name="name" placeholder="category name" />
                     </div>


<p>
           
<hr>

                    <div>
                       <button type="submit" class="addbtn"> Add Category</button>

                    </div>

</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
