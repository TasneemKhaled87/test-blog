@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Category {{$category->name}}</div>
<p>

                <div class="card-body">
           

                    <form action="{{route('category.update',['id'=>$category->id])}}" method="post" >
                          {{ csrf_field()}}
                      <div>
                             <label for="name">Name of Category:</label>
                          <input type="text" class="form-control" name="name" value="{{$category->name}}" />
                     </div>


<p>
           
<hr>

                    <div>
                       <button type="submit" class="addbtn" href="">Update Category</button>

                    </div>

</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
