
 @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Contact us :</div>
<p>

                <div class="card-body">
           
              
                @if(count ($errors) > 0)
                     <ul  class="navbar-nav mr-auto">
                         @foreach ($errors->all() as $error)
                         <li class="nav-item active">
                            {{$error}}
                         </li>
                        @endforeach
                    </ul>
                    @endif

                <!--    {{route('contactus.show')}}" -->
                    <form action="#" method="post" >
                          {{ csrf_field()}}
                      <div class="form-group">
                             <label for="name">Name :</label>
                          <input type="text" class="form-control" name="name" placeholder=" name" />
                     </div>
              <p>       
                     <div class="form-group">
                             <label for="email">Email :</label>
                          <input type="text" class="form-control" name="email" placeholder=" email" />
                     </div>

                     <p>

                      <div class="form-group">
                             <label for="message">Message:</label>
                          <input type="text" class="form-control" name="message" placeholder="message" />
                     </div>


<p>
           
<hr>

                    <div>
                       <button type="submit" class="addbtn"> Send </button>

                    </div>

</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
