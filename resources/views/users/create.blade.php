@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Users :</div>
<p>

                <div class="card-body">
           

                @if(count ($errors) > 0)
                     <ul  class="navbar-nav mr-auto">
                         @foreach ($errors->all() as $error)
                         <li class="nav-item active">
                            {{$error}}
                         </li>
                        @endforeach
                    </ul>
                    @endif


                    <form action="{{route('user.store')}}" method="post" >
                          {{ csrf_field()}}
                          <p>Please fill in this form to create an account on library</p>
                      <div class="form-group">
                             <label for="name">Name of user:</label>
                          <input type="text" class="form-control" name="name"  />
                     </div>
                     <div class="form-group">
                             <label for="email">Email:</label>
                          <input type="text" class="form-control" name="email" placeholder=" enter mail" />
                     </div>
             
                     <div class="form-group">
                             <label for="email">VarifiedEmail:</label>
                          <input type="text" class="form-control" name="email" placeholder=" varified email" />
                     </div>
                     <div class="form-group">
                             <label for="password">Password :</label>
                          <input type="text" class="form-control" name="password" placeholder="password" />
                     </div>
                    


                      <div class="form-group">
                            <label for="gender">Select Gender:</label>
                             <select class="form-control" id="gender" name="gender">
                             
                                <option value="male">male</option>
                                <option  value="female">female</option>                           

                            </select>
                     </div>


<p>
           
<hr>

                    <div>
                       <button type="submit" class="addbtn"> Add User</button>

                    </div>

</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection