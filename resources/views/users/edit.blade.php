@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit User :</div>
<p>

                <div class="card-body">
           

                    <form action="{{route('user.update',['id'=>$user->id])}}" method="post" >
                          {{ csrf_field()}}
         
                    <p>Update your account on library</p>

                      <div class="form-group">
                             <label for="name">Name of user:</label>
                          <input type="text" class="form-control" name="name" value="{{$user->name}}" />
                     </div>
                     <div class="form-group">
                             <label for="email">Email:</label>
                          <input type="text" class="form-control" name="email" placeholder=" enter mail" value="{{$user->email}}"  />
                     </div>
             
                     <div class="form-group">
                             <label for="email">VarifiedEmail:</label>
                          <input type="text" class="form-control" name="email" placeholder=" varified email" value="{{$user->varified_at}}" />
                     </div>
                     <div class="form-group">
                             <label for="password">Password :</label>
                          <input type="text" class="form-control" name="password" placeholder="password" value="{{$user->password}}" />
                     </div>
                    


                      <div class="form-group">
                            <label for="gender">Select Gender:</label>
                             <select class="form-control" id="gender" name="gender" value="{{$user->gender}}" >
                             
                                <option value="male" >male</option>
                                <option value="female">female</option>                           

                            </select>
                     </div>


<p>
           
<hr>
<p>
           
<hr>

                    <div>
                       <button type="submit" class="addbtn" href="">Edit User</button>

                    </div>

</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
