@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Users:</div>
<p>

                <div class="card-body">
           

                <table class="table table-borderless table-dark">
                       <thead>
                          <tr>
                              <th scope="col">Avater</th>
                              <th scope="col">No</th>
                              <th scope="col">Email</th>
                              <th scope="col">gender</th>
                              <th scope="col">Admin </th>
                              <th scope="col">Edit</th>
                              <th scope="col">Delete</th>
 
                          </tr>
                     </thead>
                     <tbody>
                        @foreach ($users as $user)
                         <tr>
                         <th scope="row">
                           <img src="https://www.google.com/search?q=avatar+image&client=firefox-b-1-d&sxsrf=ACYBGNQbE9jJl3CCAxB-O9d_mOROXlULWw:1576355600371&tbm=isch&source=iu&ictx=1&fir=gnzAlFgJH0wErM%253A%252C8EUQ-n_-BWubKM%252C_&vet=1&usg=AI4_-kRl9uV-8htD9hlaxdrLyHBdt9uB1w&sa=X&ved=2ahUKEwjU6-Wc_rXmAhUOoRQKHQ4QA-oQ9QEwAnoECAoQMw&biw=1299&bih=639#" alt="" class="img-thumbnail" width="50px" height="50px">
                         </th>

                              <th scope="row">{{$user->name}}</th>
                              <th scope="row">{{$user->email}}</th>
                              <th scope="row">{{$user->gender}}</th>

                              <td>                              
                                  @if($user->admin)
                                     <a class="" href="{{route('user.not.admin',['id'=>$user->id])}}">delete admin</a>

                                    @else
                                    <td>                              
                                     <a class="" href="{{route('user.admin',['id'=>$user->id])}}">make admin</a>
                                @endif
                               </td>

                               <td>                              
                               <a class="" href="{{route('user.edit',['id'=>$user->id])}}"><i class="fas fa-edit"></li>Edit</a>

                               </td>

                               <td>
                               <a class="" href="{{route('user.delete',['id'=>$user->id])}}"><i class="far fa-trash-alt"></i>Delete</a>

                               </td>
                         </tr>
                        @endforeach
                     </tbody>
                </table>




                </div>
            </div>
        </div>
    </div>
</div>
@endsection
