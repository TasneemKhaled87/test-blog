<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'auth'], function() {

//route of book 
Route::get('/books', 'BookController@index')->name('books');
Route::post('/book/update/{id}', 'BookController@update')->name('book.update');
Route::get('/book/delete/{id}', 'BookController@destroy')->name('book.delete');
Route::get('/book/edit/{id}', 'BookController@edit')->name('book.edit');
Route::get('/book/create', 'BookController@create')->name('book.create');
Route::post('/book/store', 'BookController@store')->name('book.store');

//route of category
Route::get('/categories', 'CategoryController@index')->name('categories');
Route::get('/category/edit/{id}', 'CategoryController@edit')->name('category.edit');
Route::get('/category/delete/{id}', 'CategoryController@destroy')->name('category.delete');
Route::get('/category/create', 'CategoryController@create')->name('category.create');
Route::post('/category/store', 'CategoryController@store')->name('category.store');
Route::post('/category/update/{id}', 'CategoryController@update')->name('category.update');

//route of author
Route::get('/authors', 'AuthorController@index')->name('authors');
Route::get('/author/edit/{id}', 'AuthorController@edit')->name('author.edit');
Route::get('/author/delete/{id}', 'AuthorController@destroy')->name('author.delete');
Route::get('/author/create', 'AuthorController@create')->name('author.create');
Route::post('/author/store', 'AuthorController@store')->name('author.store');
Route::post('/author/update/{id}', 'AuthorController@update')->name('author.update');

//route of user
Route::get('/users', 'UsersController@index')->name('users');
Route::get('/user/edit/{id}', 'UsersController@edit')->name('user.edit');
Route::get('/user/delete/{id}', 'UsersController@destroy')->name('user.delete');
Route::get('/user/create', 'UsersController@create')->name('user.create');
Route::post('/user/store', 'UsersController@store')->name('user.store');
Route::post('/user/update/{id}', 'UsersController@update')->name('user.update');
//route admin
Route::get('/user/admin/{id}', 'UsersController@admin')->name('user.admin');
Route::get('/user/notadmin/{id}', 'UsersController@notAdmin')->name('user.not.admin');

//route contact us
//Route::get('/contactus', 'ContactUsController@contactUS');
Route::get('/contactus','ContactUsController@show')->name('contactus.show');
Route::post('/contactus','ContactUsController@sendEmail')->name('contactus.send');
Route::post('/contactus/store','ContactUsController@store')->name('contactus.store');

//route of photo
//Route::get('/photos', 'PhotoController@index');
//Route::post('/photo/store', 'PhotoController@store');

//route settings
Route::get('/settings', 'SettingController@settings')->name('settings');
Route::post('/settings/update', 'SettingController@update')->name('settings.update');

});
//profile
Route::get('/tasneem',function(){
    return App\Profile::find(1)->profile;

})->name('tasneem');
